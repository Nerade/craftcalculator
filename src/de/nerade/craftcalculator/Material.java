package de.nerade.craftcalculator;

public class Material {
	
	private String Name;
	private Integer Amount;
	
	public Material(String name, Integer amount) {
		Name = name;
		Amount = amount;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public Integer getAmount() {
		return Amount;
	}

	public void setAmount(Integer amount) {
		Amount = amount;
		}	
}
