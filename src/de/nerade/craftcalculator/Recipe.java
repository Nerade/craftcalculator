package de.nerade.craftcalculator;

import java.util.HashMap;

public class Recipe {
	private String name;
	private HashMap<Recipe,Integer> materials;

	public Recipe(String name, HashMap<Recipe,Integer> materials){
		this.name = name;
		this.materials = materials;
	}
	
	public Recipe(String name){
		this.name = name;
		materials = new HashMap<Recipe,Integer>();
	}

	public HashMap<Recipe,Integer> getMaterials() {
		return materials;
	}

	public void setMaterials(HashMap<Recipe,Integer> materials) {
		this.materials = materials;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
