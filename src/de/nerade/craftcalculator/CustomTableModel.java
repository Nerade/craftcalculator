package de.nerade.craftcalculator;

import java.util.HashMap;
import java.util.Map.Entry;

import javax.swing.table.AbstractTableModel;

public class CustomTableModel extends AbstractTableModel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Recipe root;
	private HashMap<String,Integer> dataSet;
	
	public CustomTableModel(Recipe recipe){
		root = recipe;
		dataSet = new HashMap<String,Integer>();
		calculate(root);
	}
	
	private void calculate(Recipe material){
		for(Entry<Recipe,Integer> ele:material.getMaterials().entrySet()){
			if(ele.getKey().getMaterials().size()!=0){
				calculate(ele.getKey());
				System.out.println("Ne oder?!");
			} else {
				System.out.println(ele);
				if(!dataSet.containsKey(ele.getKey().getName())){
					System.out.println(ele);
					dataSet.put(ele.getKey().getName(), ele.getValue());
				} else {
					Integer tmp = dataSet.get(ele.getKey().getName());
					tmp += ele.getValue();
				}
			}
		}
		/*for(Integer amount : dataSet.values()){
			amount *= material.getAmount();
			System.out.println(amount);
		}*/
	}
	
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return 9;
	}

	@Override
	public String getColumnName(int column) {
		if(column == 1){
			return root.getName();
		} else {
			return "Gesamt";
		}
	}

	@Override
	public Object getValueAt(int x, int y) {
		if(x<dataSet.size()){
			if(y == 0){
				return dataSet.keySet().toArray()[x];
			} else {
				return dataSet.values().toArray()[x];
			}
		} else {
			return null;
		}
		
	}

}
