package de.nerade.craftcalculator;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.JTableHeader;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.EbeanServer;
import com.avaje.ebean.EbeanServerFactory;
import com.avaje.ebean.config.ServerConfig;

import de.nerade.craftcalculator.Recipe;
import de.nerade.craftcalculator.CustomTableModel;


public class CraftCalculator implements ActionListener{
	
	private JFrame myFrame;
	//private Map<> inputSet;
	private List<JComboBox<String>> materials;
	private List<JTextField> amounts;
	private JComboBox<String> name;
	private JPanel resultPanel;
	private JButton calculate;
	public List<Recipe> recipeStorage;
	public EbeanServer database;
	private PersistenceManager pman;

	public CraftCalculator(){
		
		myFrame = new JFrame("CraftCalculator v0.1 by Nerade");
        JPanel contentPanel = new JPanel(new BorderLayout());
        JPanel inputPanel = new JPanel(new GridLayout(0,3,10,5));
        resultPanel = new JPanel(new BorderLayout());
        
        pman = new PersistenceManager(this);
        database = pman.prepareEbeanServer();
        pman.getDatabaseRecipes();
        
        inputPanel.add(new JLabel("Rezeptname:"));
        name = new JComboBox<String>();
        name.setEditable( true );
        name.setSelectedItem( "Eintrag" );
        name.setMaximumRowCount( 10 );        
        inputPanel.add(name);
        inputPanel.add(new JLabel(""));
        
        KeyListener tfKeyListener = new KeyAdapter() {
            public void keyPressed(KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_ENTER)
                    calculate.doClick();
            }
        };
       
        materials = new ArrayList<JComboBox<String>>();
        amounts = new ArrayList<JTextField>();
        for(int i = 0; i<9;i++){
        	JComboBox<String> cur = new JComboBox<String>();
        	cur.setEditable(true);
        	cur.setMaximumRowCount( 10 );
        	JTextField cur2 = new JTextField();
        	inputPanel.add(new JLabel(new StringBuilder("Zutat #").append(i+1).toString()));
        	inputPanel.add(cur);
        	cur2.addKeyListener(tfKeyListener);
        	inputPanel.add(cur2);
        	materials.add(cur);	
        	amounts.add(cur2);
        }
        
        calculate = new JButton("Berechne");
        calculate.setActionCommand("calculate");
        calculate.addActionListener(this);
        
        JButton saveButton = new JButton("Berechne und Speicher");
        saveButton.setActionCommand("save");
        saveButton.addActionListener(this);
        
        inputPanel.add(new JLabel());
        inputPanel.add(saveButton);
        inputPanel.add(calculate);
        
        contentPanel.add(inputPanel,BorderLayout.NORTH);
        contentPanel.add(resultPanel,BorderLayout.SOUTH);
        
        myFrame.setSize(600,600);
        
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - myFrame.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - myFrame.getHeight()) / 2);
        myFrame.setLocation(x, y);     
        
        myFrame.addKeyListener(tfKeyListener);
        myFrame.add(contentPanel);
        myFrame.pack();
        myFrame.setVisible(true);
		
	}

	public static void main(String[] args) {
 
        new CraftCalculator();
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		
		Recipe newRecipe = new Recipe((String)name.getSelectedItem());
		newRecipe.setName((String)name.getSelectedItem());
		System.out.println(newRecipe.getName());
		int i = 0;
		for(JComboBox<String> ele: materials){
			String name = (String) ele.getSelectedItem();
			if(name != null){
				System.out.println(name);
				Integer amount = Integer.valueOf(amounts.get(i).getText());
				newRecipe.getMaterials().put(new Recipe(name),amount);
				//System.out.println(newRecipe.getMaterials().get(0).getName());
			}
			i++;
		}
		
		JTable result = new JTable(new CustomTableModel(newRecipe));
		JTableHeader header = result.getTableHeader();
		resultPanel.removeAll();
		resultPanel.add(header,BorderLayout.NORTH);
		resultPanel.add(result,BorderLayout.SOUTH);
		myFrame.pack();
		
		if("calculate".equals(e.getActionCommand())){

		} else if("save".equals(e.getActionCommand())){
			Bean newBean = new Bean();
			newBean.setName(newRecipe.getName());
			pman.encodeMaterials(newBean, newRecipe.getMaterials());
			Ebean.save(newBean);
			
		}
	}
}
