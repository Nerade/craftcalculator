package de.nerade.craftcalculator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="recipes")
public class Bean {

	@Id
	String name;
	@Column
	String materials;
		
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRawMaterials() {
		return materials;
	}
	public void setRawMaterials(String materials) {
		this.materials = materials;
	}
}
